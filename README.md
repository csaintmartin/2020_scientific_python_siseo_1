# Scientific Python @ SISEO

## Program
The module has 6 half days. From a pre-established skeleton, the training gradually becomes oriented to meet the specific needs / ideas of the group.

* Half Day 1, 2, 3: Introduction to Python: Work Environments: Spyder, Jupyter. Basic programming in Python. Scientific and graphic libraries for the scientist Numpy, Scipy, Pandas and Matplotlib. Various examples.
* Half day 4: Creation of a documented library with Sphinx in collaborative mode with Git and GitLab.
* Half-days 5: Themes à la carte. Examples: advanced graphics for publications with Matplotlib (complex graphics, Latex couplings), image processing, optimization, solving differential equations, Machine / Deep Learning (Scikit Learn, PyTorch) …
* Half-day 6: realization of a project: each participant proposes to solve a problem with the proposed methods. At the end of this session, the project is realized and returned as a GitLab bookstore or repository.

