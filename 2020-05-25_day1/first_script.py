# AN INTRODUCTION TO PYTHON

l = [1, 2, 3] # A Python list

# MATHEMATICAL OPERATIONS ON LISTS
l2 = []
for i in range(len(l)):
    l2.append(l[i] * 2)

# SAME WITH NUMPY
import numpy as np
a = np.array([1,2,3])
a2 = a*2

# FANCIER NUMPY
b = np.zeros((3,3))
c = np.arange(10)
d = np.linspace(0., 10., 101)


# ARRAYS AS IMAGES
import matplotlib.pyplot as plt

im = np.random.rand(100).reshape(10, 10)

plt.figure()
plt.imshow(im)
plt.show()
