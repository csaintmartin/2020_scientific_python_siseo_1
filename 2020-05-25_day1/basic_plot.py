# BASIC PLOT
import numpy as np
import matplotlib.pyplot as plt
import math

# DEFINE A MATHEMATICAL FUNCTION
def myFunc(x, a, b):
    """
    A mathematical function.
    """
    return np.exp(-x / a) * np.cos(b * x)
    """
    # PURE PYTHON SOLUTION
    out = []
    for xv in x:
        out.append(math.exp(-xv / a) * math.cos(b * xv))
    return out
    """

# EVALUATE THE FUNCTION
x = np.linspace(0., 10., 101) # Values of x

# PLOT IT
plt.figure() # FIGURE: space to plot stuff
for a in [1, 2,10]:
    y = myFunc(x, a = a ,b=3)
    plt.plot(x, y, "-", label = "a={0}".format(a)) # PLOT: plot y vs x
plt.xlabel("Axis $x$")
plt.ylabel("Axis $y$")
plt.title(r"Myfunc: $\exp(\frac{-x}{a}) \cos(x b)$" )
plt.grid()
plt.legend()
plt.savefig("basic_plot.pdf")
plt.show() # Shows the plot